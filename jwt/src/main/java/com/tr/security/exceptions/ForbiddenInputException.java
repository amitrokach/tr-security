package com.tr.security.exceptions;

/**
 * Forbidden input exception
 * @author: sigals
 * @since 27/06/2016.
 */
public class ForbiddenInputException extends Exception{
    private static final long serialVersionUID = -64496303121862837L;

    public ForbiddenInputException(String message) {
        super(message);
    }

}
