package com.tr.security.exceptions;

/**
 * Exception for bad requests from end user
 * User: sigals
 * Date: 16/08/2016
 */
public class BadRequestException extends Exception{

    public BadRequestException(String message) {
        super(message);
    }
}
