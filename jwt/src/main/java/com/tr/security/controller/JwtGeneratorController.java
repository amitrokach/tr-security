package com.tr.security.controller;

import com.tr.security.component.JwtTokenUtil;
import com.tr.security.component.JwtUser;
import com.tr.security.dao.UserRepository;
import com.tr.security.entities.JwtAuthenticationResponse;
import com.tr.security.entities.UsernamePasswordRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

@Controller
public class JwtGeneratorController extends BasicController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    UserRepository userRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UsernamePasswordRequest usernamePasswordRequest) {

        if(StringUtils.isEmpty(usernamePasswordRequest.getPassword()) || StringUtils.isEmpty(usernamePasswordRequest.getUsername())) {
            logger.info("User tried to login without supplying credentials");
            return ResponseEntity.badRequest().body("User haven't supply credentials to perform JWT generation. user credentials: " + usernamePasswordRequest);
        }

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(usernamePasswordRequest.getUsername());

        //TODO[UM] - implement here all the authentication (Ldap / Local DB)
        //TODO[UM] - implement here all the authentication (Ldap / Local DB)
        //TODO[UM] - implement here all the authentication (Ldap / Local DB)

        //TODO[UM] - Need to change the way we're validating local user in future features - Encrypt password
        //TODO[UM] - Need to change the way we're validating local user in future features - Encrypt password
        //TODO[UM] - Need to change the way we're validating local user in future features - Encrypt password
        if(usernamePasswordRequest.getPassword().equals(userDetails.getPassword())) {
            final String token = jwtTokenUtil.generateToken(userDetails);
            // Return the token
            boolean isFirstLogin = false;
            if(userDetails instanceof JwtUser) {
                Date lastLogin = ((JwtUser) userDetails).getLastLogin();
                if(lastLogin == null) {
                    logger.info("First login of user: {} to the system", usernamePasswordRequest.getUsername());
                    isFirstLogin = true;
                }
            }
            return ResponseEntity.ok(new JwtAuthenticationResponse(token, isFirstLogin));
        } else {
            return forbiddenAction403("Incorrect username or password");
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}