package com.tr.security.controller;

import com.tr.security.entities.TrApiResponse;
import com.tr.security.exceptions.BadRequestException;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Basic functionality for a controller
 * Created by ran on 01/11/15.
 */
//TODO[UM]: duplicate class from detection center - need to put this class in central place (thetary-core/common?) and remove from here
//TODO[UM]: duplicate class from detection center - need to put this class in central place (thetary-core/common?) and remove from here
//TODO[UM]: duplicate class from detection center - need to put this class in central place (thetary-core/common?) and remove from here
public abstract class BasicController {

    protected static final String UNEXPECTED_ERROR_MSG = "Unexpected error";

//    @Value("${audit.dir}")
//    private String auditDir;

    protected abstract Logger getLogger();

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity<TrApiResponse> error500(Exception e, String message) {
        getLogger().error(message, e);
        return processException(e, HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), TrApiResponse.StatusCode.Error);
    }

    @ResponseStatus( value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = BadRequestException.class)
    protected ResponseEntity<TrApiResponse> badRequest400(BadRequestException e) {
        return processException(e, HttpStatus.BAD_REQUEST, e.getMessage(), TrApiResponse.StatusCode.Error);
    }


//    @ResponseStatus(value = HttpStatus.FORBIDDEN)
//    @ExceptionHandler(value = ForbiddenInputException.class)
//    protected ResponseEntity<TrApiResponse> forbiddenInput403(ForbiddenInputException e, String remoteIp, String requestUri) throws IOException {
//
////        AuditRecord auditRecord =
////                new AuditRecord(
////                        "Forbidden parameters",
////                        new Timestamp(System.currentTimeMillis()),
////                        remoteIp == null ? "" : remoteIp,
////                        InetAddress.getLocalHost().toString(),
////                        // todo: missing user
////                        "Unknown",
////                        null,
////                        requestUri);
//
//        //TODO[UM] - add auditing when adding audit feature
//        //TODO[UM] - add auditing when adding audit feature
//        //TODO[UM] - add auditing when adding audit feature
//        //auditRecordWriter.writeAuditRecord(auditRecord, auditDir);
//        return processException(e, HttpStatus.FORBIDDEN, e.getMessage(), TrApiResponse.StatusCode.Forbidden);
//    }


    @ResponseStatus(value = HttpStatus.OK)
    protected ResponseEntity<TrApiResponse> executeActionAndPutResultInTriResponse(Supplier action) {

        Function<Supplier, ResponseEntity<TrApiResponse>> function = (t) -> {
            TrApiResponse trApiResponse = new TrApiResponse(TrApiResponse.StatusCode.Ok);
            trApiResponse.put("result", t.get());
            return new ResponseEntity<>(trApiResponse, HttpStatus.OK);
        };
        return function.apply(action);
    }


    protected ResponseEntity<TrApiResponse> okResponse200(String message) {
        TrApiResponse trApiResponse = new TrApiResponse(TrApiResponse.StatusCode.Ok);
        trApiResponse.setMessage(message);
        return new ResponseEntity<>(trApiResponse, HttpStatus.OK);

    }


    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    protected ResponseEntity<TrApiResponse> forbiddenAction403(String message) {
        TrApiResponse trApiResponse = new TrApiResponse(TrApiResponse.StatusCode.Forbidden);
        trApiResponse.setMessage(message);
        return new ResponseEntity<>(trApiResponse, HttpStatus.FORBIDDEN);
    }


    private ResponseEntity<TrApiResponse> processException(
            Exception e, HttpStatus httpStatus, String error, TrApiResponse.StatusCode statusCode) {
        return processException(e, httpStatus, error, Optional.empty(), statusCode);
    }

    /**
     * Process an exception and return the standard API response
     * @param e Thrown exception
     * @param httpStatus HTTP status of the HTTP request
     * @param error Error for the response
     * @param message Optional message for the response
     * @param statusCode Status code for the response
     * @return API response
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private ResponseEntity<TrApiResponse> processException(
            Exception e, HttpStatus httpStatus, String error, Optional<String> message, TrApiResponse.StatusCode statusCode) {
        //audit.error("Error", e);
        TrApiResponse trApiResponse = new TrApiResponse(statusCode);
        message.ifPresent(trApiResponse::setMessage);
        trApiResponse.setError(error);
        return new ResponseEntity<>(trApiResponse, httpStatus);
    }

//    /**
//     * Execute an action and return the standard API reponse
//     * @param action Action to execute
//     * @param statusCode HTTP status for the HTTP request
//     * @return Response for the API
//     */
//    private ResponseEntity<TrApiResponse> executeAndReturn(Execute action, TrApiResponse.StatusCode statusCode) {
//        return executeAndReturn(action, statusCode, Optional.empty(), HttpStatus.OK);
//    }

//    /**
//     * Execute an action and return the standard API reponse
//     * @param action Action to execute
//     * @param message Optional message for the response
//     * @param statusCode HTTP status for the HTTP request
//     * @return Reponse for the API
//     */
//    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
//    private ResponseEntity<TrApiResponse> executeAndReturn(
//            Execute action, TrApiResponse.StatusCode statusCode, Optional<String> message, HttpStatus httpStatus) {
//        action.execute();
//        TrApiResponse trApiResponse = new TrApiResponse(statusCode);
//        message.ifPresent(trApiResponse::setMessage);
//        return new ResponseEntity<>(trApiResponse, httpStatus);
//    }

    protected String getFileAsString(String path) throws IOException {

        // Join to one long string as the new line symbols interfere with the parsing
        return Files.readAllLines(Paths.get(path)).stream()
                .collect(Collectors.joining());
    }

//    /**
//     * Return error for data not found, no action will be done
//     * @return API response
//     */
//    protected ResponseEntity<TrApiResponse> dataNotFound404() {
//        // Send a supplier, which does nothing
//        return dataNotFoundAction404(() -> {});
//    }

//    protected ResponseEntity viewDataAndReturn(
//            DataViewFunction function,
//            @RequestBody(required = false) ReaderFilterJsonDef filter, Long id, Integer skip, Integer limit, String url) {
//        try {
//            DataReaderJsonDef response = function.apply(id, skip, limit, filter, url);
//            return executeActionAndPutResultInTriResponse(() -> response);
//
//        } catch (DataNotFoundException e) {
//            return dataNotFound404();
//        } catch (UnprocessableEntityException e) {
//            return unProcessAbleEntity422(e);
//        } catch (DataReaderException e) {
//            return error500(e, "Failed to read data");
//        }
//    }

    @FunctionalInterface
    protected interface GetTakenNamesByPrefixFunction {
        Set<String> apply(String prefix);
    }

//    /**
//     * Functional interface for the data view method
//     * The method receives:
//     * Id of entity to view
//     * How many lines to skip (i.e. start point)
//     * How many lines to bring for viewing
//     * Filter for the view
//     * Url for data to read
//     */
//    @FunctionalInterface
//    protected interface DataViewFunction{
//        DataReaderJsonDef apply(Long id, Integer skip, Integer limit, ReaderFilterJsonDef filter, String dataReaderUrl)
//                throws DataNotFoundException, UnprocessableEntityException, DataReaderException ;
//    }
//
//    /**
//     * Functional interface to query a data field/feature by ID, if the field/feature is not found DataNotFoundException
//     * is thrown
//     * @param <T> Class type of data field/feature
//     */
//    protected interface QueryFieldFunction<T extends FieldBase> extends ReadOrDataNotFoundFunction<T>{}
//
//    protected interface QueryDataSourceFunction<T extends DataSource> extends ReadOrDataNotFoundFunction<T> {}
//
//    @FunctionalInterface
//    protected interface PreconditionCheckFunction<T extends DataSource> {
//        void apply(T dataSource) throws DataNotFoundException, PreconditionFailedException;
//    }
//
//    @FunctionalInterface
//    protected interface SupplierThatThrows<T> {
//        T apply() throws Exception;
//    }
//
//    @FunctionalInterface
//    protected interface SendToQueueFunction {
//        boolean apply(String msg, String queueName);
//    }
}
