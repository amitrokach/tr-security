package com.tr.security.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan("com.tr.common.models.security")
@EnableJpaRepositories(basePackages = {"com.tr.security.dao"})
public class JwtConfig {

}
