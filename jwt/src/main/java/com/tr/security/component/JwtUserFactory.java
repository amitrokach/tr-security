package com.tr.security.component;

import com.tr.common.models.security.User;

class JwtUserFactory {

    private JwtUserFactory() {
    }

    static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUserName(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                user.getLastLogin()

                //mapToGrantedAuthorities(user.getAuthorities()),
//                user.getEnabled(),
//                user.getLastPasswordResetDate()
        );
    }
}
