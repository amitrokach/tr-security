package com.tr.security.entities;

public class JwtAuthenticationResponse {
    private final String token;
    private final boolean isFirstLogin;

    public JwtAuthenticationResponse(String token, boolean isFirstLogin) {
        this.token = token;
        this.isFirstLogin = isFirstLogin;
    }

    public String getToken() {
        return this.token;
    }

    public boolean isFirstLogin() {
        return isFirstLogin;
    }
}
