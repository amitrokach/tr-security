package com.tr.security.entities;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;

public class TrApiResponse extends HashMap<String, Object> {
    private static final long serialVersionUID = 7083556881719883520L;

    private static final String STATUS = "status";
    private static final String MESSAGE = "MESSAGE";
    private static final String TIMESTAMP = "time";
    private static final String HOST = "host";

    public TrApiResponse() {
        setTimestamp(null);
        String hostname;
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            hostname = "Unresolved";
        }
        put(HOST, hostname);
    }

    public TrApiResponse(StatusCode retCode) {
        this();
        setRetCode(retCode);
    }

    public void setOk() {
        setRetCode(StatusCode.Ok);
    }

    public boolean isOk() {
        return get(STATUS).equals(StatusCode.Ok);
    }

    public void setError(String message) {
        setRetCode(StatusCode.Error);
        put(MESSAGE, message);
    }

    private void setRetCode(StatusCode retCode) {
        put(STATUS, retCode);
    }

    public String getMessage() {
        return (String) get(MESSAGE);
    }

    public void setMessage(String msg) {
        put(MESSAGE, msg);
    }

    private void setTimestamp(Date dt) {
        if (dt == null) {
            dt = new Date();
        }
        put(TIMESTAMP, dt.toString());
    }

    public boolean isError() {
        return get(STATUS).equals(StatusCode.Error);
    }

    public enum StatusCode {
        Ok, Error, BadData, ErrorObjectNotFound, ErrorRequestUrlNotFound, ErrorGatewayNotFound, PartialSuccess,
        Unauthorized, PreconditionFailed, Forbidden
    }
}
