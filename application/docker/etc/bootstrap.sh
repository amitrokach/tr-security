#!/bin/bash

pwd=`pwd`
echo $pwd

export JAVA_OPTS="$DC_JAVA_OPTS $JAVA_OPTS"

ADD_TO_COMMAND_PROFILE=""
if [ "$PROFILES" != "" ];
then
    ADD_TO_COMMAND_PROFILE="--spring.profiles.active=$PROFILES"
fi

# Set TR_VERSION
if [ "$TR_VERSION" == "" ]; then
   echo "WARNING: Missing TR_VERSION environment variable!"
fi

if [[ $DB_TYPE = "oracle" ]];
then
   export DB_USER=$ORACLE_USER
   export ENCODED_PASSWORD=$ORACLE_PASSWORD
   export DB_URL=$ORACLE_URL
   export DB_DIALECT="org.hibernate.dialect.Oracle10gDialect"
else
   export ENCODED_PASSWORD="ENC(TBcGj6GTjiwwQRzf8rYNuA==)"
   export DB_TYPE="postgresql"
fi

echo TR_VERSION=$TR_VERSION > /opt/tr/version.properties

echo "Checking servers..."

#Check Postgres availability
SERVER=tr-postgres
PORT=5432

`nc -z -v -w5 $SERVER $PORT &> /dev/null`
result1=$?
retry_count=5
sleep_time=5
while [  "$result1" != 0 ] && [ "$retry_count" -gt 0 ]
do
	echo  "port $PORT on $SERVER is closed. trying again"
	sleep $sleep_time
	`nc -z -v -w5 $SERVER $PORT &> /dev/null`
	result1=$?
	let retry_count=retry_count-1
done
if [  "$result1" != 0 ]
then
	echo "Could not reach $PORT on $SERVER. Quitting application"
	exit -1
fi
echo "Port $PORT on $SERVER is open!!"

echo "Starting tr-security application"

java $JAVA_OPTS -jar ./tr-security.jar $ADD_TO_COMMAND_PROFILE --spring.config.location=/opt/tr/tr-security/config/application.properties &