package com.tr.security.app;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@ComponentScan({"com.tr.security"})
@Import(WebSecurityConfig.class)
public class SecurityServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityServiceApplication.class, args);
    }
}
